//Codigo del profesor

const io = require('../io');

require('dotenv').config();
//require para usar la libreria request-json
const requestJson = require("request-json");
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechulnvdb9ed/collections/";
const nameUserBD = "accounts";
const apiKey = "apiKey=" + process.env.MLAB_API_KEY;

function getAccountsByUserId(req, res) {
  console.log("Invocado GET /apitechu/v2/getAccountsByUserId/:id");

  console.log("La id del usuario a buscar es " + req.params.id);
  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"userId":' + req.params.id + '}';
  var consulta = nameUserBD + "?" + query + '&' + apiKey;
  httpClient.get(consulta,
    function (err, resMlab, body) {
      //si hay error, devuelvo status 500
      if (err) {
        var response = {
          "msg": "Error obteniendo cuentas del usuario"
        }
        res.status(500)
      } else { //si lo encuentro, es 200 pero no lo pongo xq es el q va por defecto
        if (body.length > 0) {
          var response = body; //devuelvo una lista con las cuentas
        } else { //no lo he encontrado

          var response = { "msg": "Error obteniendo las cuentas del usuario" }
          res.status(404); //indico el código de no lo he encontrado
        }
      }
      res.send(response);
    });
}

module.exports.getAccountsByUserId = getAccountsByUserId;