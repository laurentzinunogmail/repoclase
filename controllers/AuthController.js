//Codigo del profesor

const io = require('../io');

require('dotenv').config();
//nos traemos la libreria crypt
const cryptLibrary = require('../crypt');
//require para usar la libreria request-json
const requestJson = require("request-json");
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechulnvdb9ed/collections/";
const nameUserBD = "user";
const apiKey = "apiKey=" + process.env.MLAB_API_KEY;

//Codigo del profesor
function loginV1(req, res) {
  console.log("POST /apitechu/v1/login");

  var users = require('../usuarios.json');
  for (user of users) {
    if (user.email == req.body.email
      && user.password == req.body.password) {
      console.log("Email found, password ok");
      var loggedUserId = user.id;
      user.logged = true;
      console.log("Logged in user with id " + user.id);
      io.writeUserDataToFile(users);
      break;
    }
  }

  var msg = loggedUserId ?
    "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

  var response = {
    "mensaje": msg,
    "idUsuario": loggedUserId
  };

  res.send(response);
}

//
function loginV2(req, res) {
  console.log("Invocado POST /apitechu/v2/login");

  //iniciamos el cliente contra la url de la base de datos mlab
  var httpClient = requestJson.createClient(baseMlabURL); +
    //NOS TRAEMOS EL USUARIO
    console.log("El usuario a buscar es " + req.body.email);
  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"email":"' + req.body.email + '"}';
  var consulta = nameUserBD + "?" + query + '&' + apiKey;
  httpClient.get(consulta,
    function (err, resMlab, body) {
      //si hay error, devuelvo status 500
      if (err) {
        var response = {
          "msg": "Error genérico ;)"
        }
        res.status(500)
      } else { //si lo encuentro, es 200 pero no lo pongo xq es el q va por defecto
        if (body.length > 0) {
          //hemos encontrado el usuario. 
          //comprobamos el password 
          if (cryptLibrary.checkPassword(req.body.password, body[0].password)) {
            //el login es correcto. 
            // var query = "";
            var putBody = '{"$set":{"logged":true}}';
            var fullQuery = nameUserBD + "?" + query
              + "&"
              + apiKey;
            httpClient.put(fullQuery, JSON.parse(putBody));
            var response = {
              "msg": "Usuario logado con éxito",
              "id": body[0].id
            };

          } else {
            var response = { "msg": "Error de login ;)" }
            res.status(401);
          }

        } else { //no lo he encontrado

          var response = { "msg": "Error de login ;)" }
          res.status(401); //indico el código de no lo he encontrado
        }
      }
      res.send(response);
    })
}

//Codigo del profesor
function logoutV1(req, res) {
  console.log("POST /apitechu/v1/logout");

  var users = require('../usuarios.json');
  for (user of users) {
    if (user.id == req.params.id && user.logged === true) {
      console.log("User found, logging out");
      delete user.logged
      console.log("Logged out user with id " + user.id);
      var loggedoutUserId = user.id;
      io.writeUserDataToFile(users);
      break;
    }
  }

  var msg = loggedoutUserId ?
    "Logout correcto" : "Logout incorrecto";

  var response = {
    "mensaje": msg,
    "idUsuario": loggedoutUserId
  };

  res.send(response);
}

function logoutV2(req, res) {
  console.log("POST /apitechu/v2/logout/:id");

  //iniciamos el cliente contra la url de la base de datos mlab
  var httpClient = requestJson.createClient(baseMlabURL); +
    //NOS TRAEMOS EL USUARIO
    console.log("El usuario a buscar es " + req.params.id);
  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"id":' + req.params.id + '}';
  var consulta = nameUserBD + "?" + query + '&' + apiKey;
  httpClient.get(consulta,
    function (err, resMlab, body) {
      //si hay error, devuelvo status 500
      if (err) {
        var response = {
          "msg": "Error genérico de logout ;)"
        }
        res.status(500)
      } else { //si lo encuentro, es 200 pero no lo pongo xq es el q va por defecto
        if (body.length > 0) {
          //hemos encontrado el usuario. Deslogamos 
          var putBody = '{"$unset" : {"logged" : ""}}';
          var fullQuery = nameUserBD + "?" + query
            + "&"
            + apiKey;
          httpClient.put(fullQuery, JSON.parse(putBody));
          var response = { "id": body[0].id };

        } else { //no lo he encontrado

          var response = { "msg": "Error de logout ;)" }
          res.status(401); //indico el código de no lo he encontrado
        }
      }
      res.send(response);
    })

  // var query = "";
  // var putBody = '{"$set" : {"logged" : true}}';
  // httpClient.put(nameUserBD + "?" +
  //   + query
  //   + "&"
  //   + mLabApikey, JSON.parse(putBody));


  //LOGOUT

  //var putBody = '{"$unset" : {"logged" : ""}}';
}

module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;

module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;