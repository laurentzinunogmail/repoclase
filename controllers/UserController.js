require('dotenv').config();
//hacemos referencia a nuestra  libreria io
const io = require('../io')
//nos traemos la libreria crypt
const crypt = require('../crypt');
//require para usar la libreria request-json
const requestJson = require("request-json");
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechulnvdb9ed/collections/";
const nameUserBD = "user";
const apiKey = "apiKey=" + process.env.MLAB_API_KEY;

//Refactorizado método de getUsers del GET
function getUsersV1(req, res) {
  console.log("Invocado GET /apitechu/v1/users");
  console.log("parametro top: " + req.query.$top);
  console.log("parametro count: " + req.query.$count);
  //cargo parametros
  var top = req.query.$top;
  var count = req.query.$count;
  var objReturn = {};

  //cargo usuarios
  var users = require('../usuarios2.json');
  //console.log(users);
  var cuantos = users.length;

  if (count == "true") {
    objReturn.count = cuantos;
    console.log("cuantos: " + cuantos);
  }

  objReturn.users = top ? users.slice(0, top) : users;
  res.send(objReturn);
}

function getUsersV2(req, res) {
  console.log("Invocado GET /apitechu/v2/users");

  //iniciamos el cliente contra la url de la base de datos mlab
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");
  httpClient.get(nameUserBD + "?" + apiKey,
    function (err, resMlab, body) {
      var response = !err ? body : {
        "msg": "Error obteniendo usuarios"
      };
      res.send(response);
    }
  );
}

function getUserbyIdV2(req, res) {
  console.log("Invocado GET /apitechu/v1/users/:id");

  console.log("La id a buscar es " + req.params.id);
  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"id":' + req.params.id + '}';
  var consulta = nameUserBD + "?" + query + '&' + apiKey;
  httpClient.get(consulta,
    function (err, resMlab, body) {
      //si hay error, devuelvo status 500
      if (err) {
        var response = {
          "msg": "Error obteniendo usuario"
        }
        res.status(500)
      } else { //si lo encuentro, es 200 pero no lo pongo xq es el q va por defecto
        if (body.length > 0) {
          var response = body[0]; //devuelvo el body, pero solo el elemento, no la lista con un elemento
        } else { //no lo he encontrado

          var response = { "msg": "Error obteniendo usuario" }
          res.status(404); //indico el código de no lo he encontrado
        }
      }
      res.send(response);
    }

  );

}

//Refactorizado método de deleteUsers del DELETE
function deleteUserV1(req, res) {
  console.log("Invocado DELETE /apitechu/v1/users/:id");

  console.log("La id a borrar es " + req.params.id);
  //cargo usuarios
  var users = require('../usuarios2.json');

  /*FOR NORMAL*/
  for (var i = 0; i < users.length; i++) {
    if (users[i].id == req.params.id) {
      console.log(users[i].id);
      //borramos el usuario
      users.splice(i, 1);
      console.log("Usuario borrado");
      break;
    }

  }
  io.writeUserDataToFile(users);

}

//Refactorizado método de createUser del POST
function createUserV1(req, res) {
  console.log("Invocado POST /apitechu/v1/users");
  //vamos a recibir info / mandarla con cabeceras
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  };
  //cargamos los usuarios
  var users = require("../usuarios2.json");
  //agregamos el nuevo usuario
  users.push(newUser);
  console.log("Usuario añadido");

  io.writeUserDataToFile(users);

  res.send(users);
}

//Refactorizado método de createUser del POST
function createUserV2(req, res) {
  console.log("Invocado POST /apitechu/v2/users");
  //vamos a recibir info / mandarla con cabeceras
  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);

  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password) //paso el hash del password
  };

  var httpClient = requestJson.createClient(baseMlabURL);
  var query = nameUserBD + '?' + apiKey;
  httpClient.post(query, newUser,
    function (err, resMlab, body) {
      if (resMlab.statusCode != 200) {
        console.log("error " + resMlab.body.message);
        res.status(500).send({ "msg": "error: " + resMlab.body.message });
      }
      else {
        console.log("Usuario guardado con éxito");
        res.status(201).send({ "msg": "Usuario guardado con éxito" });
      }
    }

  );
}

function loginV1(req, res) {

  console.log("Invocado POST /apitechu/v1/login");
  //vamos a recibir info / mandarla con cabeceras

  console.log(req.body.email);
  console.log(req.body.password);

  //Buscar el usuario con email igual al enviado:
  // Si no está el email -> NOK.
  // Si está el email:
  // Si el password es igual al enviado -> OK
  // Si el password no es igual al enviado -> NOK
  // Si es login correcto:

  var user = {
    "email": req.body.email,
    "password": req.body.password
  };

  //cargamos los usuarios
  var users = require("../usuarios2.json");

  //Buscamos al usuario con el email igual al enviado
  console.log("Usando Array findIndex");
  var indexOfElement = users.findIndex(
    function (element) {
      console.log("comparando " + element.email + " y " + req.body.email);
      return element.email == req.body.email
    }
  )
  //si lo hemos encontrado
  if (indexOfElement != -1) {
    console.log("encontrado usuario");
    //verifico si la contraseña es la correcta
    var loginOk = users[indexOfElement].password == req.body.password;

  }
  else {
    //NO ESTÁ, DEVOLVEMOS ERROR
    console.log("no encontrado");
    res.send("NOK user no encontrado");
  }
  if (!loginOk) { res.send({ "msg": "Login incorrecto" }); }
  else {
    //es ok, devuelvo ok
    res.send({ "msg": "Login correcto", "idUsuario": users[indexOfElement].id })
    //agregamos propiedad logged a true
    users[indexOfElement].logged = true;
    io.writeUserDataToFile(users);
  }
}

function logoutV1(req, res) {

  console.log("Invocado POST /apitechu/v1/logoutV1/:id");
  //vamos a recibir info / mandarla con cabeceras

  //cargamos los usuarios
  var users = require("../usuarios2.json");

  //Buscamos al usuario con el email igual al enviado
  console.log("Usando Array findIndex");
  var indexOfElement = users.findIndex(
    function (element) {
      console.log("comparando " + element.id + " y " + req.params.id);
      return element.id == req.params.id
    }
  )

  if (indexOfElement != -1) {
    console.log("encontrado usuario, intentamos deslogar");
    if (users[indexOfElement].logged) {
      //el usuario estaba logado, deslogamos
      delete users[indexOfElement].logged;

      //guardamos en bd
      io.writeUserDataToFile(users);
      //devolvemos logout correcto
      res.send({ "msg": "Logout correcto", "idUsuario": users[indexOfElement].id })

    }
    else {
      res.send({ "msg": "Logout incorrecto, usuario no estaba logado" })

    }
  }
  else {
    //No encontrado usuario
    res.send({ "msg": "Logout incorrecto, usuario no" });

  }
}
//identifico lo que voy a sacar fuera, que se identifica como indica a la derecha.
//sin esta linea no se podrá enlazar desde fuera
module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserbyIdV2 = getUserbyIdV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;

