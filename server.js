//librerias van en constantes. nos traemos el framework
const express = require('express');
//lo ponemos en marcha
const app = express();

//hacemos referencia a nuestra nueva libreria
const io = require('./io')
//hacemos referencia a el controlador de usuarios
const userController = require('./controllers/UserController')
const authController = require('./controllers/AuthController')
const accountController = require('./controllers/AccountController')

//manejador para poder hacer peticiones desde cualquier origen. lo metemos mas abajo como manejador.
var enableCORS = function (req, res, next){
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  //esto permite recibir la cabecera content-type
  res.set("Access-Control-Allow-Headers", "Content-Type")
  next();
}

//le pasamos una capa de middleware para que use el parseador JSON (para que el body se muestre bien)
app.use(express.json());
//si se usa jwt y se usa cabecera

// le pasamos otra capa de middleware. Hay una plitica que impide que se hagan peticiones desde
// una url a otra. esto permite que suceda estas llamadas
app.use(enableCORS);

//variable de entorno. Si el PORT No existe, cojo el 3000. Si existe pone el port.
//es un OR IZQ OR DCHA. EXISTE IZQ? SI--> LO PONGO. NO? --> DCHA.
const port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto " + port);


//funcion de lista de usuarios basica.
//pasamos la variable con la funcion, no paso parametros
app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserbyIdV2);


//Vamos a hacer un handler para los delete. paso por parametro el id del usuario a borrar
//TODO: CAMBIAR A AUTHCONTROLLER Y VERIFICAR FUNCIONAMIENTO
app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);

//Vamos a hacer un handler para los post. quiero meter en el body el usuario que quiero agregar
//TODO: CAMBIAR A AUTHCONTROLLER Y VERIFICAR FUNCIONAMIENTO
app.post('/apitechu/v1/users', userController.createUserV1);

app.post('/apitechu/v2/users', userController.createUserV2);

//Vamos a hacer un handler para los post. quiero meter en el body el usuario que quiero agregar
//TODO: CAMBIAR A AUTHCONTROLLER Y VERIFICAR FUNCIONAMIENTO
app.post('/apitechu/v1/login', userController.loginV1);
app.post('/apitechu/v2/login', authController.loginV2);

//TODO: CAMBIAR A AUTHCONTROLLER Y VERIFICAR FUNCIONAMIENTO
app.post('/apitechu/v1/logoutV1/:id', userController.logoutV1);
app.post('/apitechu/v2/logout/:id', authController.logoutV2);

//seccion de cuentas

app.get('/apitechu/v2/accounts/:id', accountController.getAccountsByUserId);

//funcion de lista de usuarios basica
//app.get('/apitechu/v1/users',
//    function(req,res) {
//      console.log("Invocado GET /apitechu/v1/users");
//construyo la respuesta, __dirname es una constante que
//indica el path del directorio donde se ejecuta el script en ese momento
//res.sendFile('usuarios.json',{root: __dirname});

//cargo el json directamente. users es de tipo array (el json empieza por corchete)
// require carga cosas. modulos o ficheros o mas cosas.
//      var users = require('./usuarios.json');
//      res.send(users);
//    }
//)

app.get('/apitechu/v1/hello',
  function (req, res) {
    //peticion del cliente-->req
    //res respuesta al cliente
    console.log("Invocado GET /apitechu/v1/hello");
    //construyo la respuesta, el JSON directamente entre llaves
    res.send(
      { "msg": "Hola desde API TechU" }
    );
  }

)

//creo una petición post que contemple dos parámetros
app.post('/apitechu/v1/monstruo/:p1/:p2',
  function (req, res) {

    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

  }
)